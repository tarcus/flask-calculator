from math import sqrt


class Calculator:
    def __init__(self, max_results=8):
        self.results = []
        self.max_results = max_results
        self.functions = dict()
        self.operations = dict()

    def add_result(self, name):
        def decorator(f):
            def wrapped(x, y):
                self.results.insert(0, f(x, y))
                if len(self.results) > self.max_results:
                    self.results.pop(-1)
            self.functions[f.__name__] = wrapped
            self.operations[f.__name__] = name
            return wrapped
        return decorator


calc = Calculator()


@calc.add_result("Sum")
def add_sum(x, y):
    result = x + y
    return f'{x} + {y} = {result}'


@calc.add_result("Difference")
def add_diff(x, y):
    result = x - y
    return f'{x} - {y} = {result}'


@calc.add_result("Multiply")
def add_multiply(x, y):
    result = x * y
    return f'{x} * {y} = {result}'


@calc.add_result("Division")
def add_divide(x, y):
    result = x / y if y != 0 else 'NaN'
    return f'{x} / {y} = {result}'


@calc.add_result("Power")
def add_power(x, y):
    result = x ** y
    return f'{x} ** {y} = {result}'


@calc.add_result("Square root")
def add_sqrt(x, y=0):
    result = sqrt(x) if x >= 0 else '"Complex numbers is out of scope"'
    return f'sqrt({x}) = {result}'


if __name__ == '__main__':
    print(calc.functions)
    calc.functions['add_sum'](5, 3)
    print(calc.results)
    print(calc.operations['add_sum'])