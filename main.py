import flask as fl
from calculate import calc

app = fl.Flask(__name__)


@app.route("/")
def home():
    return fl.render_template('base.html', results=calc.results, operations=calc.operations)

@app.route("/calculate/<method>", methods=["POST"])
def calculate(method):
    form = fl.request.form
    x = int(form['x']) if form['x'] else 0
    y = int(form['y']) if form['y'] else 0
    calc.functions[method](x, y)
    return fl.redirect("/")


if __name__ == '__main__':
    app.run(debug=True)
